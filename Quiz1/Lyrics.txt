Lyrics "my life "


Sometimes I look at myself it's 
A little bit selfish
I tried but I can't win 
You don't even know 
How could somebody look at me 
And think that I'm happy 
I haven't been laughing 
Too much anymore 

You say I seem okay when I'm sitting here with all my friends
Easy to say when you're looking from the outside in
I'll never change, loving myself is all pretend
And I might never again

I tried everything to please you
I forgot to think of me too
I woke up and realized 
That this is my life 

And every time that I felt hopeless
I fucked myself up to keep on going 
And I never realized
That this is my life

Sometimes I wish I was different 
And someone would listen 
It's hard it explain it 
You don't even know 
Last time I told you I missed you 
It turned to an issue 
Guess I was mistaken
I'll leave you alone 

You say I seem okay when I'm sitting here with all my friends
Easy to say when you're looking from the outside in
I'll never change, loving myself is all pretend
And I might never again

I tried everything to please you
I forgot to think of me too
I woke up and realized 
That this is my life 

And every time that I felt hopeless
I fucked myself up to keep on going 
And I never realized
That this is my life