
import { Link } from "react-router-dom";

/*function Headder () {

    return (
        <div>
            Welcome To Website : &nbsp;&nbsp;&nbsp;
                <Link to ="/">Calcular Number</Link>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to ="about">CREATER</Link>
            <hr />
        </div>
    );
}

export default Headder;*/


import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

function Headder() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            Welcome To Website : &nbsp;&nbsp;&nbsp;
                    <Link to ="/">Calcular Number</Link>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to ="about">CREATER</Link>
                <hr />
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Headder;