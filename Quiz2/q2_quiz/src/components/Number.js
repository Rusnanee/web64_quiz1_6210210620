function Number (props) {

    return (
        <div>
            <h2> Calculating numbers odd or even </h2>
            <h3> User Name {props.name}</h3>
            <h3> The Number Is {props.num} </h3>
            <hr />
            <h1> The Result Is {props.result} </h1>
        </div>
    );
}

export default Number;