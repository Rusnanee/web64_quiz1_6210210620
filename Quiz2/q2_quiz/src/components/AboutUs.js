import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { width } from '@mui/system';

function AboutUs (props) {

    return (
        <Box sx= {{ width:"100%" }}>
        <Paper elevation={1}>

            <h3> **************************************** CREATE BY **************************************** </h3>

            <h3> Name : {props.name} </h3>
            <h3> Student ID : {props.id} </h3>
            <h3> Studying : {props.study} </h3>
            <h3> Department Of : {props.department} </h3>
            <h3> Faculty of : {props.faculty} </h3>
            <h3> University : {props.university} </h3>

            <h3> ******************************************************************************************* </h3>

        </Paper>
        </Box>
    );
}

export default AboutUs;