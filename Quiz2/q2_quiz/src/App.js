import logo from './logo.svg';
import './App.css';

import NumberCalPage from './pages/NumberCalPage';
import AboutUsPage from './pages/AboutUsPage';
import Headder from './components/Headder';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Headder />
      <Routes>
        <Route path="/" element={
          <NumberCalPage />
        } />
        <Route path="about" element={
          <AboutUsPage />
        } />
      </Routes>
    </div> 
  );
}

export default App;