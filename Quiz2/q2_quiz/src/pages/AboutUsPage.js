

import AboutUs from "../components/AboutUs";
function AboutUsPage () {
    return (
        <div>
            <div alige="center">
                <AboutUs name="Rusnanee Yusoh"
                         id="6210210620"
                         study="computer science"
                         department="Computational Science"
                         faculty="Science"
                         university="Prince of Songkla University" />
            </div>
        </div>
    );
}

export default AboutUsPage;