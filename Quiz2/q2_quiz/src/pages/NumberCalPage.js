
import Number from "../components/Number";
import { useState } from "react";

import Button from '@mui/material/Button';

function NumberCalPage () {

    const [ name , setName ] = useState("");
    const [ num , setNum ] = useState("");
    const [ ans , setAns ] = useState("");
    const [ result , setResult ] = useState("");
    

    function CalculatNum () {
        
        let n = parseFloat(num);

        if (n == 0) {
            setResult ("Zero");
        }
        else if (n % 2 == 0) {
            setResult ("Even");
        }
        else if (n % 2 == 1) {
            setResult ("Odd");
        }
    }

    return (
        <div alige="left">
            <div alige="center">
                Welcome To Website Calculate Number
                <hr />

                Please Enter User Name : <input type="text" 
                                             value={name}
                                             onChange={ (e) => { setName(e.target.value);  } } />
                <br />
                Please Enter Number : <input type="text" 
                                             value={num}
                                             onChange={ (e) =>{ setNum(e.target.value); } } />
                <br /><br />
                <Button variant="contained" onClick={ (e) => { CalculatNum () } } > Calculate </Button>

                { result != 0 &&
                    <div>
                        <hr />
                        <Number 
                            name={name}
                            num={num}
                            result={result} />
                        <hr />
                    </div>
                }
            </div>
        </div>
    );
}

export default NumberCalPage;